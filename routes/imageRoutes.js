'use strict'

var express=require('express');
var api=express.Router();
//controlador
var ImageController=require('../controllers/imageController');

var multipart=require('connect-multiparty');
var multipartMiddleware=multipart({uploadDir:'./uploads'});

//rutas
api.get('/prueba',ImageController.prueba);
api.post('/image',ImageController.saveImage);
api.get('/image/:id',ImageController.getImage);
api.get('/images/:albumId?',ImageController.getImages);
api.put('/image/:id',ImageController.updateImage);
api.delete('/image/:id',ImageController.deleteImage);
api.post('/upload-image/:id', multipartMiddleware ,ImageController.uploadImage);
api.get('/get-image-file/:imageFile',ImageController.getImageFile);

module.exports=api;