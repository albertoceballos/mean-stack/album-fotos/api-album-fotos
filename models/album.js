'use strict'

var mongoose = require('mongoose');
var schema = mongoose.Schema;

var AlbumSchema = schema({
    title: String,
    description: String,
});

module.exports = mongoose.model('Album', AlbumSchema);