'use strict'

var mongoose = require('mongoose');
var schema = mongoose.Schema;

var ImageSchema = schema({
    title: String,
    description: String,
    picture: String,
    album: { type: schema.ObjectId, ref: 'Album' },
    created_at: Date,
});

module.exports = mongoose.model('Image', ImageSchema);