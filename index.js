'use strict'

var mongoose = require('mongoose');
var app=require('./app');
mongoose.set('useFindAndModify', false);
mongoose.connect('mongodb://localhost:27017/album', { useNewUrlParser: true })
    .then(() => {
        console.log('La conexión con la BBDD está funcionando correctamente');
        app.listen(3700,()=>{
            console.log('Servidor corrriendo correctamente en localhost');
        })
    })
    .catch(err => console.log(err));