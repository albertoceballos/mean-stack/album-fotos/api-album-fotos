'use strict'

var Album = require('../models/album');

//extarer todos los albumes
function getAlbums(req, res) {
    Album.find({},(err, albums) => {
        if (err) {
            res.status(500).send({ message: 'Error en la petición' });
        } else {
            if (albums) {
                res.status(200).send({ albums });
            } else {
                res.status(404).send({ message: 'No hay albumes' });
            }
        }
    });
}

//extraer un album
function getAlbum(req, res) {
    var albumId = req.params.id;
    Album.findById(albumId, (err, album) => {
        if (err) {
            res.status(500).send({ message: 'Error en la petición' });
        } else {
            if (album) {
                res.status(200).send({ album });
            } else {
                res.status(400).send({ message: 'No existe el álbum' });
            }
        }
    });
}

//Guardar nuevo álbum
function saveAlbum(req, res) {
    var params = req.body;
    var title = params.title;
    var description = params.description;
    if (title) {
        var album = new Album();
        album.title = title;
        if(description){
            album.description=description;
        }else{
            album.description=null;
        }
        album.save((err, albumStored) => {
            if (err) {
                res.status(500).send({ message: 'Error al guardar el álbum' });
            } else {
                if (albumStored) {
                    res.status(200).send({ message: 'Álbum guardado con éxito', album: albumStored });
                } else {
                    res.status(404).send({ message: 'No se ha podido guardar el álbum' });
                }
            }
        });
    } else {
        res.status(400).send({ message: 'Faltan datos, introduce el título' });
    }
}

//actualizar album
function upadateAlbum(req,res){
    var albumId=req.params.id;
    var params=req.body;
    if(params){
        Album.findByIdAndUpdate(albumId,params,{new:true},(err,albumUpdated)=>{
            if(err){
                res.status(500).send({message:'Error al intentar guardar el álbum'});
            }else{
                if(albumUpdated){
                    res.status(200).send({message:'Álbum actualizado con éxito',album:albumUpdated});
                }else{
                    res.status(404).send({message:'No se ha podido actualizar el álbum'});
                }
            }
        });
    }else{  
        res.status(404).send({message:'Faltan datos, introduce datos para poder actualizar'});
    }
}

//borrar album
function deleteAlbum(req,res){
    var albumId=req.params.id;
    Album.findByIdAndRemove(albumId,(err,albumDeleted)=>{
        if(err){
            res.status(500).send({message:'Error al borrar álbum'});
        }else{
            if(albumDeleted){
                res.status(200).send({message:'Álbum borrado con éxito',album:albumDeleted});
            }else{
                res.status(404).send({message:'No se pudo borrar el álbum'});
            }
        }
    });
}


module.exports = {
    getAlbums,
    getAlbum,
    saveAlbum,
    upadateAlbum,
    deleteAlbum,
};