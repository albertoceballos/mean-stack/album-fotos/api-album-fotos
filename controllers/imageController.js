'use strict'

var Image=require('../models/image');
var Album=require('../models/album');

var fs=require('fs');
var path=require('path');

function prueba(req,res){
    res.status(200).send({message:'Hola desde imageController'});
}

//guardar imagen
function saveImage(req,res){
   var params=req.body;
   var image=new Image();
   image.title=params.title;
   image.description=params.description;
   image.picture=params.picture;
   image.album=params.album;
   image.picture=null;
   image.created_at=Date.now();
   image.save((err,imageStored)=>{
    if(err){
        res.status(500).send({message:'Error al guardar la imagen'});
    }else{
        if(imageStored){
            res.status(200).send({message:'Imagen guradada con éxito', image:imageStored})
        }else{
            res.status(400).send({message:'No se pudo guardar la imagen'});
        }
    }
   });
}

//extraer imagen
function getImage(req,res){
    var imageId=req.params.id;
    Image.findById(imageId,(err,image)=>{
        if(err){
            res.status(500).send({message:'Error al hacer la petición'})
        }else{
            if(image){
                res.status(200).send({image});
            }else{
                res.status(404).send({message:'No existe la imagen'});
            }
        }
    }).populate('album');
}

//extraer imágenes
function getImages(req,res){
    var albumId=req.params.albumId;
    if(albumId){
        //si llega el id saca las imágenes de ese álbum concreto
        Image.find({album:albumId},(err,images)=>{
             if(err){
                res.status(500).send({message:'Error al hacer la petición'});
            }else{
                if(images){
                    res.status(200).send({images});
                }else{
                    res.status(404).send({message:'No hay imágenes'});
                }
             }   
        }).sort('title');
    }else{
        //si no llega el id del album, saca todas las imágenes de la BBDD
        Image.find({},(err,images)=>{
            if(err){
                res.status(500).send({message:'Error al hacer la petición'});
            }else{
                if(images){
                    res.status(200).send({images});
                }else{
                    res.status(404).send({message:'No hay imágenes'});
                }
            }
        }).sort('title');
    }
}

//actualizar imagen
function updateImage(req,res){
    var imageId=req.params.id;
    var params=req.body;
    Image.findByIdAndUpdate(imageId,params,{new:true},(err,imageUpdated)=>{
        if(err){
            res.status(500).send({message:'Error al hacer la petición'});
        }else{
            if(imageUpdated){
                res.status(200).send({message:'Imagen actualizada con éxito', image:imageUpdated});
            }else{
                res.status(404).send({message:'No se pudo actualizar la imagen'});
            }
        }
    })
}

//eliminar imagen
function deleteImage(req,res){
    var imageId=req.params.id;
    Image.findByIdAndRemove(imageId,(err,deletedImage)=>{
        if(err){
            res.status(500).send({message:'Error al borrar la imagen'});
        }else{
            if(deletedImage){
                res.status(200).send({message:'Imagen borrada con éxito',image:deletedImage});
            }else{
                res.status(404).send({message:'No se ha podido eliminar la imagen'});
            }
        }
    });
}

//subir imágenes
function uploadImage(req,res){
    var imageId=req.params.id
    if(req.files){
        var file_path=req.files.image.path;
        var file_split=file_path.split('\\');
        var file_name=file_split[1];
        console.log(file_name);
        Image.findByIdAndUpdate(imageId,{picture:file_name},{new:true},(err,imageUpdated)=>{
            if(err){
                res.status(500).send({message:'Error al subir la imagen'});
            }else{
                if(imageUpdated){
                    res.status(200).send({message:'Imagen subida con éxito',image:imageUpdated});
                }else{
                    res.status(404).send({message:'No se ha podido actualizar la imagen'});
                }
            }
        });
    }else{
        res.status(200).send({message:'No has subido ninguna imagen'});
    }
}

//extraer la imagen en crudo para mostrar
function getImageFile(req,res){
    var imageFile=req.params.imageFile;
    fs.exists('./uploads/'+imageFile,(exists)=>{
        if(exists){
            res.sendFile(path.resolve('./uploads/'+imageFile));
        }else{
            res.status(200).send({message:'No existe la imagen'});
        }
    });
}





module.exports={
    prueba,
    saveImage,
    getImage,
    getImages,
    updateImage,
    deleteImage,
    uploadImage,
    getImageFile,
};